import { Component, OnInit, Input } from '@angular/core';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-result-table',
  templateUrl: './result-table.component.html',
  styleUrls: ['./result-table.component.css']
})
export class ResultTableComponent implements OnInit {

  keyword: string = '';

  allCategories: string[] = [];

  filteredCategories: string[] = [];

  constructor(private categoryService: CategoryService) { 
  }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories() {
    this.categoryService.getCategories()
      .subscribe(categories => this.allCategories = categories);
  }

  searchCategories() {
    this.filteredCategories = this.allCategories.filter(category => category.toLowerCase().includes(this.keyword.toLowerCase()));
  }
}
